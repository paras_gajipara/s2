<?php
include ('flipkartapi.php');
include('amazon.php');
include('CombineClass.php');
//include('ebay.php');
function fatch_flipkart($ser)
{
	
	$search=$ser;
	$search=str_replace(" ", "+", $search);
	//Replace <affiliate-id> and <access-token> with the correct values
	$flipkart = new \Futurerulers\Flipkart("parasgaji", "eb5bf5ff71cd47c5bb188892a198e45c", "json");

	//To view category pages, API URL is passed as query string.
	$url ='https://affiliate-api.flipkart.net/affiliate/search/json?query='.$search.'&resultCount=10';
	//URL is base64 encoded to prevent errors in some server setups.
	//This parameter lets users allow out-of-stock items to be displayed.
	$hidden = isset($_GET['hidden'])?false:true;
	//Call the API using the URL.
	$details = $flipkart->call_url($url);
	if(!$details){
		echo 'Error: Could not retrieve products list.';
		exit();
	}

	//The response is expected to be JSON. Decode it into associative arrays.
	$details = json_decode($details, TRUE);

	//The response is expected to contain these values.
	$FLIPKART_products = $details;
	//flipkart products are returned
	return $FLIPKART_products;
}
function fatch_amazon($ser)
{
	$search=$ser;
	$search=str_replace(" ", "+", $search);
	//Calling method of Amazon class
	$obj=new Amazon($search);
	$url=$obj->amz($search);
	
	//The responce is expected tobe XML.Converting and decode it into JASON and then Associative Array
	$xml = file_get_contents($url,0,null,null);
	$simpleXml=new SimpleXMLElement($xml);
	$json = json_encode($simpleXml);
	$AMAZON_products = json_decode($json,TRUE);
	if(!$AMAZON_products){
		echo 'Error: Could not retrieve products list.';
		exit();
	}
	$AMAZON_products = $AMAZON_products['Items'];
	//returning the products
	return $AMAZON_products;
}

	
function fatch_ebay($ser)
{
	$ebay = new ebay('unityinf-cfd0-4b54-b66a-33ac43e0de8c', 'EBAY-IN');
	$sort_orders = $ebay->sortOrders();
	$EBAY_products = $ebay->findItemsAdvanced($_POST['search'], BestMatch);
	if(!$EBAY_products){
		echo 'Error: Could not retrieve products list.';
		exit();
	}
	return $EBAY_products;
	
	}

function combine($srch)
{
	 
	$f_pro=fatch_flipkart($srch);
	$items = array();
    $i = 0; 
    foreach($f_pro["productInfoList"] as $item) {
        $items[$i] = new FlipkartProduct($item['productBaseInfo']);
        $i++;
    }
    $a_pro=fatch_amazon($srch);
    if(!empty($a_pro))
    {
    	$a_pro=$a_pro['Item'];
    	foreach ($a_pro as $item) {
        $items[$i] = new AmazonProduct($item);
        $i++;
    }
	}
    if( isset($_SESSION['items'] ))
        unset($_SESSION['items']);
    $_SESSION['items'] = serialize($items);
    return $items; 

}
function searchById($pid , $products){
    foreach ($products as $product  ){
        if ( $pid == $product->getProductId()) {
            return $product;
        }
    }
}


?>