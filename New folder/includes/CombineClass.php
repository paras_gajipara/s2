<?php 
	class Product {
		protected $product_Price;
		protected $product_Id;
		protected $product_Title;
		protected $image_Url;
		protected $product_Url;
		protected $product_Description;
		function getproductPrice() {
			return $this->product_Price;
		}
		
		function getproductId() {
			return $this->product_Id;
		}
		function getproductTitle() {return $this->product_Title;}
		function getimageUrl() {return $this->image_Url;}
		function getproductUrl() {return $this->product_Url;}
		function getproductDescription() {return $this->product_Description;}
	}
	class FlipkartProduct extends Product
	{
		function __construct($product)
		{
			$this->product_Price = array();
			$this->product_Id = array();
			$this->product_Url = array();
			$this->product_Price["flipkart"] = $product["productAttributes"]["sellingPrice"]["amount"];
			$this->product_Id["flipkart"] = $product["productIdentifier"]["productId"];
			$this->product_Title = $product["productAttributes"]["title"];
			$this->image_Url = $product["productAttributes"]["imageUrls"]["200x200"];
			$this->product_Url["flipkart"] = $product["productAttributes"]["productUrl"];
			$this->product_Description = $product["productAttributes"]["productDescription"];
			
		}
		function getproductPrice() {return $this->product_Price["flipkart"];}
		function getproductId(){return $this->product_Id["flipkart"];}
		function getproductUrl(){return $this->product_Url["flipkart"];}
	}

	class AmazonProduct extends Product
	{
		function __construct($product)
		{
			$this->product_Price = array();
			$this->product_Id = array();
			$this->product_Url = array();
			$this->product_Id["amazon"] = $product["ASIN"];
			$this->product_Url["amazon"] = $product["DetailPageURL"];
			$this->image_Url = $product["LargeImage"]["URL"];
			$this->product_Price["amazon"] = ( $product["OfferSummary"]["LowestNewPrice"]["Amount"] ) / 100;
			$this->product_Title = $product["ItemAttributes"]["Title"];
			$x = '<ul>';
			foreach ($product['ItemAttributes']['Feature'] as $feature) {
				$x = $x."<li>".$feature."</li>";
			}
			$x = $x."</ul>";
			$this->product_Description = $x;
		}
		function getproductPrice() {return $this->product_Price["amazon"];}
		function getproductId(){return $this->product_Id["amazon"];}
		function getproductUrl(){return $this->product_Url["amazon"];}
	}
?>