<?php
error_reporting(0);?><!-- Footer -->
					<div class="navbar navbar-default navbar-sm navbar-fixed-bottom">
						<ul class="nav navbar-nav no-border visible-xs-block">
							<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
						</ul>

						<div class="navbar-collapse collapse" id="navbar-second">
							<div class="navbar-text">
								<a href="https://sv.co/">StartUp Village</a> | 2016.
							</div>

							<div class="navbar-right">
								<ul class="nav navbar-nav">
									<li><a href="#"><i class="icon-facebook"></i> Facebook</a></li>
									<li><a href="#"><i class="icon-google-plus"></i> Google+</a></li>
									<li><a href="#"><i class="icon-github"></i> Github</a></li>	
								</ul>
							</div>
						</div>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
