<!DOCTYPE html>
<html lang="en">
  <head>
    
    <title>Startup Village</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link href='css/css.css' rel='stylesheet' type='text/css'>


    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/modernizr.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">

  </head>

  <body>

    <div
      class="fb-like"
      data-share="true"
      data-width="450"
      data-show-faces="true">
    </div>

    <div class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="img/3.jpg">
            </div>
            <div class="item">
              <img src="img/4.jpg">
            </div>
            <div class="item">
              <img src="img/5.jpg">
            </div>
            <div class="item">
              <img src="img/6.jpg">
            </div>
            <div class="item">
              <img src="img/8.png">
            </div>
            <div class="item">
              <img src="img/9.jpg">
            </div>
            <div class="item">
              <img src="img/11.jpg">
            </div>
        </div>
    </div>

<!-- Remeber to put all the content you want on top of the slider below the slider code -->
    
  <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">

            <div class="inner">
              <br>
              <!--<h1 class="masthead-brand" id="logo">
                <a href="index.html">
                  <img src="img/logo.png" alt="StartUp" height="110" width="120" />
                </a>
              </h1>-->
              <!--<nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#">Features</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </nav>-->
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading"><span class="label label-primary">Search your product</span></h1>
              <div class="row">
                <div class="col-lg-12">
                  <form action="main_home.php" method="POST" id="search_results">
                    <div class="input-group">
                      <input type="text" name="search" class="form-control" placeholder="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                      </span>
                    </div><!-- /input-group -->
                  </form>
                </div><!-- /.col-lg-12 -->
              </div><!-- /.row -->
              <br> 
              <a href="main_home.php" class="btn btn-lg btn-danger" >Explore the world of products</a>
             </p>
          </div>

        </div>

      </div>

    </div>

   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>