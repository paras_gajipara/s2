<?php
include("header.php");

require_once 'includes/functions.php';
// $products = unserialize($_SESSION['items']);
// $pid=$_GET['pid'];
// $product = searchById($pid , $products);

// $productId = $product->getProductId();
// $title = $product->getProductTitle();
// $productImage = $product->getImageUrl();
// $productUrl = $product->getProductUrl();
// $currency = "Rs.";
// $priceFlipkart= $product->getProductPrice();
// $productDescription = $product->getProductDescription();
?>


<!--<div class="thumbnail">
                    
                    <div class="caption-full">
                       
                    </div>
                    
                </div>-->


                <!-- Content area -->
                <div class="content">

                    <!-- Square thumbs -->
                    <h4 class="content-group text-semibold">
                        Product Name
                    </h4>
                    <div style="float:left; width:400px;height:500px; background:blue;margin-bottom:5px">
                    <hr>
                    <img class="img-responsive" src="http://placehold.it/800x300" alt="">
                    <br></div>
                    <!-- Simple panel -->
                    <div class="panel panel-flat" style="float:right">
                        <div class="panel-heading">
                            <u><h5 class="panel-title">Product Description</h5></u>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">
                            <h6 class="text-semibold">Start your development with no hassle!</h6>
                            <p class="content-group">Common problem of templates is that all code is deeply integrated into the core. This limits your freedom in decreasing amount of code, i.e. it becomes pretty difficult to remove unnecessary code from the project. Limitless allows you to remove unnecessary and extra code easily just by removing the path to specific LESS file with component styling. All plugins and their options are also in separate files. Use only components you actually need!</p>

                            <h6 class="text-semibold">What is this?</h6>
                            <p class="content-group">Starter kit is a set of pages, useful for developers to start development process from scratch. Each layout includes base components only: layout, page kits, color system which is still optional, bootstrap files and bootstrap overrides. No extra CSS/JS files and markup. CSS files are compiled without any plugins or components. Starter kit was moved to a separate folder for better accessibility.</p>

                            <h6 class="text-semibold">How does it work?</h6>
                            <p>You open one of the starter pages, add necessary plugins, uncomment paths to files in components.less file, compile new CSS. That's it. I'd also recommend to open one of main pages with functionality you need and copy all paths/JS code from there to your new page, it's just faster and easier.</p>
                        </div>
                    </div>
                    <!-- /simple panel -->

                    

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>
